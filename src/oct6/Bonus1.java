package oct6;

/*
Wskaż liczbę o 1 większą od reprezentowanej liczby przez cyfry w tablicy, np. [1,3,2,4] -> 1324 + 1 -> [1,3,2,5]
 */

import java.util.Arrays;
import java.util.Scanner;

public class Bonus1 {

    public static void main(String[] args) {

        while (true) {

            //NOTE: it was specified during class that VALUES can be NUMBERS and not just DIGITS
            //The solution can be made MUCH simpler if we just use Strings, but here I intentionally
            //chose to go through an actual array of ints


            //input
            Scanner in = new Scanner(System.in);
            System.out.println("Input ARRAY VALUES (space delimited):");
            String input = in.nextLine();

            //breaks out of the loop (basically exits the program) if the input is empty
            if (input.equals("")) {
                break;
            }

            //make our job easier
            if (input.charAt(0) == '-') {
                //if we wanted to account for negative numbers with this method,
                //we would need to write a bit of extra code to handle the "-" sign
                System.out.println("Input must be > 0; let's not complicate");
                continue;
            }


            int[] numbersFromInput = parseStringToArray(input);

            long oldValue = parseArrayToValue(numbersFromInput);
            System.out.print("oldValue = " + oldValue + ", ");

            long newValue = oldValue + 1;
            System.out.println("newValue = " + newValue);

            System.out.println("newSequenceOfDigits = " + Arrays.toString(parseValueToArray(newValue)));
        }


    }

    private static int[] parseValueToArray(long value) {
        //and we're going back
        String valueAsString = String.valueOf(value);
        int[] newSequenceOfDigits = new int[valueAsString.length()];
        for (int i = 0; i < newSequenceOfDigits.length; i++) {
            newSequenceOfDigits[i] = valueAsString.charAt(i) - '0';
        }

        return newSequenceOfDigits;
    }

    private static long parseArrayToValue(int[] numbersFromInput) {
        //normally, I'd just build a String and then parse its value
        //but here let's do some math
        long parsedValue = 0;
        int exponent = 0;
        for (int i = numbersFromInput.length - 1; i >= 0; i--) {
            int currentNumber = numbersFromInput[i];
            parsedValue += currentNumber * (long) Math.pow(10, exponent);
            if (currentNumber == 0) {
                //log10 of 0 would return infinity
                exponent++;
            } else {
                exponent += (int) Math.log10(currentNumber) + 1;
            }

        }

        return parsedValue;
    }

    private static int[] parseStringToArray(String input) {
        //first split our input into substrings, the parse each
        String[] inputMerged = input.strip().split(" ");
        int[] numbersFromInput = new int[inputMerged.length];
        for (int i = 0; i < numbersFromInput.length; i++) {
            numbersFromInput[i] = Integer.parseInt(inputMerged[i]);
            System.out.print("[" + i + "] = " + numbersFromInput[i] + ", ");
        }
        return numbersFromInput;
    }

}
