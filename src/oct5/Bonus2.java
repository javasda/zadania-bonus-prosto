package oct5;

import java.util.Scanner;

/**
 * W podanym ciągu znaków znajdź najdłuższy podciąg składający się z jednego znaku:
 * AABCDDBBBEA -> “B:3” <br>
 */

public class Bonus2 {

    public static void main(String[] args) {
        while (true) {
            Scanner in = new Scanner(System.in);

            System.out.println("Input a STRING");
            String str = in.nextLine();

            //breaks out of the loop (basically exits the program) if the input is empty
            if (str.equals("")) {
                break;
            }

            System.out.println(getRepeatingCharacter(str));

        }
    }

    private static String getRepeatingCharacter(String str) {
        //special case
        //if STR has 1 character, it's automatically the longest substring
        if (str.length() == 1) {
            return str;
        }

        //from this point on we already now that STR has at least 2 characters

        //temporary counter, how many of the *current* character have we found?
        int count = 1;
        //what was the length of the longest substring we found so far?
        int max = 0;
        //which character was that?
        char repeatCharacter = str.charAt(0);

        //iterate over every character, starting from index 1 (the 2nd character)
        //this time we compare with the *previous* character
        for (int i = 1; i < str.length(); i++) {
            if (str.charAt(i) == str.charAt(i - 1)) {
                //the substring continues
                count++;
            } else {
                //the substring ends here, so we reset the counter
                count = 1;
            }

            //is the current substring longer that the current max?
            if (count > max) {
                max = count;
                repeatCharacter = str.charAt(i - 1);
            }

        }

        //we return a String to display
        return repeatCharacter + ":" + max;
    }

}
