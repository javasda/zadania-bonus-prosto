package oct5;

import java.util.HashMap;
import java.util.Scanner;

/**
 * Napisz mechanizm szyfrujący i deszyfrujący wykorzystujący szyfr cezara
 * <br />Tekst jawny: MĘŻNY BĄDŹ, CHROŃ PUŁK TWÓJ I SZEŚĆ FLAG
 * <br />Tekst zaszyfrowany: OHBÓŻ DĆFĄ, EKTRP ŚZŃM YŹSŁ L UAGWĘ INCJ
 */
public class Bonus3 {

    //the beautiful Polish alphabet
    private static final String ALPHABET = "AĄBCĆDEĘFGHIJKLŁMNŃOÓPRSŚTUWYZŹŻ";
    //the "classic" Caesar's cipher has an offset of 3, but we could change it to w/e
    private static final int OFFSET = 3;

    //a helper class to manage encrypted alphabets
    public static class AlphabetCache {
        //this HashMap will hold exactly one "alphabet" for each "offset" we use in our program
        private static final HashMap<Integer, char[]> cache = new HashMap<>();

        //get an alphabet with the given offset (create it first if it doesn't exist yet)
        public static char[] getAlphabet(int offset) {
            //is there no ALPHABET with key==offset in our cache? build it
            //if it already exists, this line won't do anything
            cache.putIfAbsent(offset, buildOffsetAlphabet(offset));
            //now retrieve an ALPHABET that's keyed to our offset
            return cache.get(offset);
        }

        private static char[] buildOffsetAlphabet(int offset) {

            //our new alphabet (empty for now, but with the correct length)
            char[] offsetAlphabet = new char[ALPHABET.length()];

            for (int i = 0; i < ALPHABET.length(); i++) {
                //we want to shift the letters by our OFFSET,
                //but we need to make sure we don't fall out of bounds of the Array (at the last few letters)
                //to "wrap around" we use % (modulo)
                int replacementPos = (i + offset) % ALPHABET.length();
                //we populate our "encrypted" alphabet with offset characters acc. to the formula above
                offsetAlphabet[i] = ALPHABET.charAt(replacementPos);
            }
            return offsetAlphabet;
        }
    }

    public static void main(String[] args) {
        while (true) {
            Scanner in = new Scanner(System.in);

            System.out.println("Input a STRING");
            String str = in.nextLine();

            //breaks out of the loop (basically exits the program) if the input is empty
            if (str.equals("")) {
                break;
            }

            String encrypted = encrypt(str, OFFSET);
            System.out.println(encrypted);

            //the cipher is symmetrical, so we can reverse the encryption by passing -offset
            String decrypted = encrypt(encrypted, -OFFSET);
            System.out.println(decrypted);

        }
    }

    //this can be made A LOT EASIER if we didn't have to take Polish characters into account ;-)
    //without them, we can rely on the fact that ASCII codes are in order...
    private static String encrypt(String str, int offset) {
        //to make our job a little easier
        str = str.strip().toUpperCase();

        //offset 0 means the result is the same string, so no need to do anything further
        if (offset == 0) {
            return str;
        }

        //we'll put our alphabets into Arrays, just because they're a tiny bit easier to manipulate that way
        //this is the "base" alphabet
        char[] normalAlphabet;
        //this is the offset ("encrypted") alphabet
        //example: with offset 1, the first five letters will be: ĄBCĆD
        char[] offsetAlphabet;

        if (offset > 0) {
            normalAlphabet = AlphabetCache.getAlphabet(0);
            offsetAlphabet = AlphabetCache.getAlphabet(offset);
        } else {
            //we can just switch the alphabets for negative offsets
            //we could also build a "symmetrical" alphabet with offset+Alphabet.length
            //but that's more computations
            normalAlphabet = AlphabetCache.getAlphabet(Math.abs(offset));
            offsetAlphabet = AlphabetCache.getAlphabet(0);
        }

        //this Array will hold our encrypted message, we will turn it into a String at the end
        char[] encrypted = new char[str.length()];

        for (int i = 0; i < str.length(); i++) {
            //the original character at index "i"
            char originalCharacter = str.charAt(i);

            //the encrypted equivalent of our character c is put into the array also at index "i"
            encrypted[i] = getEncryptedChar(originalCharacter, normalAlphabet, offsetAlphabet);
        }

        //make a String out of our encrypted Array of characters
        return String.valueOf(encrypted);
    }

    private static char getEncryptedChar(char key, char[] normalAlphabet, char[] offsetAlphabet) {
        //we look through the "original" alphabet to find the index of our character "key"
        for (int i = 0; i < normalAlphabet.length; i++) {
            if (key == normalAlphabet[i]) {
                //return a character from our "encrypted" alphabet with the same index
                return offsetAlphabet[i];
            }
        }
        //if we can't find the character in our alphabet, it's probably a special character
        //for example: . , ! # < > (or space)
        //in that case, we just return the character unmodified
        return key;
    }
}
