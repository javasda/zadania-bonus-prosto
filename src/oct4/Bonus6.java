package oct4;

import java.util.Arrays;
import java.util.Scanner;

/**
 * Napisz program usuwający duplikaty z tablicy i zwracający nową tablicę, np.
 * Tablica wejściowa: [20, 20, 30, 40, 50, 50, 50],
 * Tablica wyjściowa: [20, 30, 40, 50]
 */

public class Bonus6 {

    public static void main(String[] args) {

        while (true) {
            Scanner in = new Scanner(System.in);

            System.out.println("Input the SIZE of the array:");
            int size = in.nextInt();

            //breaks out of the loop (basically exits the program) if the input is negative
            //not elegant, but works
            if (size < 0) {
                break;
            }

            int[] arr = new int[size];

            //populate the array
            for (int i = 0; i < arr.length; i++) {
                System.out.println("input element with index " + i);
                arr[i] = in.nextInt();
            }
            //this is the initial array
            System.out.println("Done.\n\nARRAY    = " + Arrays.toString(arr));

            int[] uniques = withoutDuplicates(arr);

            //this is the array without duplicates
            System.out.println("UNIQUE =     " + Arrays.toString(uniques));
        }

    }

    private static int[] withoutDuplicates(int[] arr) {
        //this is our "workplace"
        int[] tempArr;
        //this will contain our results
        int[] uniques;

        //special case, no need to do anything (0-1 elements don't have duplicates)
        if (arr.length <= 1) {
            uniques = arr;
        } else {
            tempArr = new int[arr.length];
            //how many unique values have we found so far
            //this doubles up as the "current index" of our "workplace" tempArr
            int uniqueCount = 0;

            //sort ascending (lowest to highest)
            //this way we can simply compare each element with the proceeding one
            Arrays.sort(arr);

            for (int i = 0; i < arr.length - 1; i++) {
                //is the VALUE the same as the next one?
                //if yes, skip it
                //if no, put the VALUE to our "workplace" at the "current" index
                //and increase the "current" index
                if (arr[i] != arr[i + 1]) {
                    tempArr[uniqueCount] = arr[i];
                    uniqueCount++;
                }
            }

            //the above loop will skip the last unique element(s)
            //so we need to add it here
            tempArr[uniqueCount] = arr[arr.length - 1];
            uniqueCount++;

            //there are "uniqueCount" unique values in tempArr
            //(the rest is initialized to 0 by default)
            //copyOf creates a new Array with size == "uniqueCount"
            //copying all the elements between index 0 and uniqueCount - 1
            uniques = Arrays.copyOf(tempArr, uniqueCount);
        }

        return uniques;
    }



}
