package oct4;

import java.util.Arrays;
import java.util.Scanner;

/**
 * Napisz program, który odwróci zawartość tablicy, np.
 *
 *     [1, 10, 4, 5, 2, 12] -> [12, 2, 5, 4, 10, 1]
 *     [1, 2, 5, 3, 10] -> [10, 3, 5, 2, 1]
 */

public class Bonus5 {

    public static void main(String[] args) {

        while (true) {
            Scanner in = new Scanner(System.in);

            System.out.println("Input ARRAY SIZE:");
            int size = in.nextInt();

            //breaks out of the loop (basically exits the program) if the input is negative
            //not elegant, but works
            if (size < 0) {
                break;
            }

            int[] arr = new int[size];

            //populate the array
            for (int i = 0; i < arr.length; i++) {
                System.out.println("input element with index " + i);
                arr[i] = in.nextInt();
            }
            //this is the initial array
            System.out.println("Done.\n\nARRAY    = " + Arrays.toString(arr));

            //seems that Objects are passed by reference in Java, so this works
            reverseInPlace(arr);

            //this is the reversed array
            System.out.println("REVERSED = " + Arrays.toString(arr));
        }

    }

    private static void reverseInPlace(int[] arr) {
        int size = arr.length;
        //we don't need to iterate over the entire array (the "right" side will be already replaced)
        //integer division rounds down
        int halfSize = size / 2;

        for (int i = 0; i < halfSize; i++) {
            //hold the "left" element, because it will be overwritten later
            int temp = arr[i];
            //this is the "right" index corresponding to the current "left" index (i)
            int revIndex = size - 1 - i;
            //the "left" element is replaced by the "right" element
            arr[i] = arr[revIndex];
            //the "right" element is replaced by our temporary value, which still holds the "left" value
            //we can't just assign arr[i] because it's already replaced!
            arr[revIndex] = temp;
        }
    }

}
