package oct5;


import java.util.Arrays;
import java.util.Scanner;

/**
 * Mając listę liczb i liczbę k, zwróć, czy dowolne dwie liczby z listy sumują się do k.
 * Na przykład, biorąc pod uwagę [10, 15, 3, 7] i k z 17, zwróć prawdę, ponieważ 10 + 7 to 17.
 */
public class Bonus1 {

    public static void main(String[] args) {

        while (true) {
            Scanner in = new Scanner(System.in);

            System.out.println("Input ARRAY VALUES (space delimited):");
            String arrString = in.nextLine();

            //breaks out of the loop (basically exits the program) if the input is empty
            if (arrString.equals("")) {
                break;
            }

            //breaks the input string into individual numbers (still represented as shorter Strings)
            String[] arrStringsParsed = arrString.split(" ");
            int valCount = arrStringsParsed.length;

            //parse our substrings into actual integers
            int[] arr = new int[valCount];
            for (int i = 0; i < valCount; i++) {
                arr[i] = Integer.parseInt(arrStringsParsed[i]);
            }

            System.out.println("Input the integer K: ");
            int k = in.nextInt();


            //this is the initial array of integers
            System.out.println("\n\nARRAY    = " + Arrays.toString(arr));

            //this function checks if any pair of values within ARR adds up to K
            if (addsUpTo(arr, k)) {
                System.out.println("YAY\t༼ つ ◕_◕ ༽つ");
            } else {
                System.out.println("NOP\t(╯°□°）╯︵ ┻━┻");
                System.out.printf("No two values add up to %d%n", k);
            }
        }
    }

    private static boolean addsUpTo(int[] arr, int k) {
        //sorts our numbers in ascending order
        Arrays.sort(arr);

        for (int i = 0; i < arr.length - 1; i++) {
            //the inner loop doesn't have to go over the *entire* array
            //it can start from the next element after "i" (the current position in the outer loop)
            for (int j = i + 1; j < arr.length; j++) {
                if (arr[i] + arr[j] == k) {
                    System.out.println(arr[i] + " + " + arr[j] + " == " + k);
                    //we found a pair that adds up to K, this ends the loops early
                    return true;
                }
            }

        }
        //we iterated over the entire array and couldn't find any pairs that add up to K
        return false;
    }
}
