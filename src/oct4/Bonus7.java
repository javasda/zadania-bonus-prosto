package oct4;

import java.util.Arrays;
import java.util.Scanner;

/**
Napisz program obliczający różnicę pomiędzy największą i najmniejszą
 wartością z tablicy.
 Tablica wejściowa: [20, 20, 30, 40, 50, 50, 50],
 wynik: 50 - 20 = 30
 */

public class Bonus7 {

    //This is just a tiny helper class that holds our results
    //You can ignore it ;-)
    private static class MinMax {
        MinMax(int min, int max) {
            this.min = min;
            this.max = max;
            this.diff = max - min;
        }
        public int min;
        public int max;
        public int diff;
    }

    public static void main(String[] args) {

        while (true) {
            Scanner in = new Scanner(System.in);

            System.out.println("Input ARRAY SIZE:");
            int size = in.nextInt();

            //breaks out of the loop (basically exits the program) if the input is negative
            //not elegant, but works
            if (size < 0) {
                break;
            }

            int[] arr = new int[size];

            //populate the array
            for (int i = 0; i < arr.length; i++) {
                System.out.println("input element with index " + i);
                arr[i] = in.nextInt();
            }
            //this is the initial array
            System.out.println("Done.\n\nARRAY    = " + Arrays.toString(arr));

            //we hold the results in our helper class
            MinMax minMax = diffMaxMin(arr);
            System.out.println(minMax.max + " - " + minMax.min + " == " + minMax.diff);
        }

    }

    private static MinMax diffMaxMin(int[] arr) {
        //special case
        if (arr.length == 0) {
            System.out.println("empty array");
            return new MinMax(0, 0);
        }

        //at first, both MIN and MAX are equal to the 1st element
        int min = arr[0];
        int max = arr[0];

        //if arr contains only 1 element (at index 0), this won't enter the loop
        //since it's not necessary (the difference of element - element is always 0)
        for (int i = 1; i < arr.length; i++) {
            //if the current element is lower than MIN, it becomes the new MIN
            if (arr[i] < min) {
                min = arr[i];
            }
            //if the current element is higher than MAX, it becomes the new MAX
            if (arr[i] > max) {
                max = arr[i];
            }
        }

        //results are stored in our helper class
        return new MinMax(min, max);
    }

}
